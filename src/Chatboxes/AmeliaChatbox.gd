extends Control

var is_complete = false
var is_finished = false
var is_choosed = false

var pname = 'Nugi'
var pcolor = '#25D366'
var uname = 'Amelia'
var ucolor = '#fa5fd8'

var udialogue = [
	[
		"Aloooo",
		"Selamat pagina",
		"Dah bangun belum ni ( '__')/",
	],
	[
		"Bosenna di rumah",
		"Jalan-jalan yuk besok",
		"Udah lama gak jalan sama Nunug",
		"Yuk?",
	],
	[
		"Kangen tau sama kamu",
		"Sibuk anet (/___\\)",
		"Mau seneng-seneng sama Nunug",
		"Gimana? (T__T)",
	],
]

var options = [
	[
		"Aloooo",
		"Morning",
		"Selamat pagina anet",
		"Udah dong \\(^o^)/",
	],
	[
		"Boleh boleh",
		"Itu Nunung tapi typo?",
		"Gak bisa kayaknya :(",
		"Mau ke mana?",
	],
	[
		"Jangan nangisna dong :( Ku liat dulu ya",
		"Iya aku juga kangen, nanti ya ku kabarin",
		"Maafna... Ku kabarin ya",
		"Sama mau seneng seneng juga :(( Aku kabarin nanti ya",
	]
]

var uresponse = [
	[
		[
			"ALOOOO WKWKWKWK",
		],
		[
			"Ohayou~",
			"Hmm dasar kita multikultural"
		],
		[
			"Anet anet wkwkwk",
		],
		[
			"Semangat anet wkwkwk",
		],
	],
	[
		[
			"Ini Ipin ya? ( 'o')/",
			"Seriusna jawab >:("
		],
		[
			"Kan Nugi jadi Nunug WKWKWK",
			"Jawabna ih, yuk",
		],
		[
			":'(((",
			"Sad anet",
		],
		[
			"(O.O) Sales tik*t dotkom ya mas?",
			"Beneranna gak nih?",
		],
	],
	[
		[
			"Gakna nangis kalo jalan-jalan",
			"Kabarinna jalan-jalan ya ( '__')/"
		],
		[
			"Kalo beneranna kangen, jalan-jalan ya",
			"Akuna tunggu ( '__')/"
		],
		[
			"Maafna mulu T_T",
			"Yaudahna aku tungguna ya",
		],
		[
			"Ote, kabarinnya ya <3",
		],
	]
]

onready var chatLog = get_node("VBoxContainer/RichTextLabel")
onready var option1 = get_node("VBoxContainer/VBoxContainer/Option1")
onready var option2 = get_node("VBoxContainer/VBoxContainer/Option2")
onready var option3 = get_node("VBoxContainer/VBoxContainer/Option3")
onready var option4 = get_node("VBoxContainer/VBoxContainer/Option4")

var ops_picked = []
var curr_dialog_sect = 0

func _process(_delta):
	if is_complete:
		if is_choosed:
			print("Amelia terpilh")
			options.append([
				"Siapa katanya yang mau jalan-jalan nih? (>////<)",
				"Jalan-jalan yaa jadinya",
				"Misi ada paket, paket jalan-jalan besok wkwkwk",
				"Besok mau ketemuan di mana?"
			])
			uresponse.append([
				[
					"AKU AKU AKU",
					"Luvna <3",
				],
				[
					"Yeyna anett",
					"Makasihna ya <3",
				],
				[
					"Paket anetttt",
					"Asik ketemuna Nunugggg"
				],
				[
					"O-----------O",
					"Jalan jalannaa? Yeyyy",
					"Ketemuna tempat biasa aja yaa, see you <3"
				],
			])
		elif !is_choosed:
			options.append([
				"Sorry ya, aku gak bisa ternyata",
				"Amelna, maaf ya gak bisa besok :(",
				"Jalan-jalannya minggu depan aja mau gak?",
				"Aku gak bisa besok jalan-jalannya nih"
			])
			ucolor = '#ababab'
			uresponse.append([
				[
					"The user has been blocked you"
				],
				[
					"The user has been blocked you"
				],
				[
					"The user has been blocked you"
				],
				[
					"The user has been blocked you"
				],
			])
		is_complete = false
		is_finished = true
		_loop()

# Called when the node enters the scene tree for the first time.
func _ready():
	_loop()

func _loop():
	_deactivate_options()
	
	if curr_dialog_sect < 3:
		var dialogue_set = udialogue[curr_dialog_sect]
		for dialogue in dialogue_set:
			add_chat_log(uname, dialogue, ucolor)
	
		var option_set = options[curr_dialog_sect]
		add_options(option_set[0], option_set[1], option_set[2], option_set[3])
		
	elif is_finished:
		var option_set = options[options.size() - 1]
		add_options(option_set[0], option_set[1], option_set[2], option_set[3])
		is_finished = false

func add_chat_log(dname, text, color):
	chatLog.append_bbcode('\n')
	chatLog.append_bbcode('[color=' + color + ']' + dname + '[/color]')
	chatLog.append_bbcode('\n')
	chatLog.append_bbcode(text)
	chatLog.append_bbcode('\n')

func add_options(op1, op2, op3, op4):
	option1.text = op1
	option2.text = op2
	option3.text = op3
	option4.text = op4
	
	option1.connect("pressed", self, "_option1_pressed")
	option2.connect("pressed", self, "_option2_pressed")
	option3.connect("pressed", self, "_option3_pressed")
	option4.connect("pressed", self, "_option4_pressed")
	
	_activate_options()

func give_response(op):
	var dialogue_set = uresponse[curr_dialog_sect][op]
	for dialogue in dialogue_set:
		add_chat_log(uname, dialogue, ucolor)
	
	curr_dialog_sect += 1
	_loop()

func _option1_pressed():
	add_chat_log(pname, option1.text, pcolor)
	ops_picked.append(1)
	_deactivate_options()
	give_response(0)

func _option2_pressed():
	add_chat_log(pname, option2.text, pcolor)
	ops_picked.append(2)
	_deactivate_options()
	give_response(1)

func _option3_pressed():
	add_chat_log(pname, option3.text, pcolor)
	ops_picked.append(3)
	_deactivate_options()
	give_response(2)

func _option4_pressed():
	add_chat_log(pname, option4.text, pcolor)
	ops_picked.append(4)
	_deactivate_options()
	give_response(3)

func _deactivate_options():
	option1.visible = false
	option2.visible = false
	option3.visible = false
	option4.visible = false

func _activate_options():
	option1.visible = true
	option2.visible = true
	option3.visible = true
	option4.visible = true
