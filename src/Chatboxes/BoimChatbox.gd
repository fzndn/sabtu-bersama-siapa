extends Control

var is_complete = false
var is_finished = false
var is_choosed = false

var pname = 'Nugi'
var pcolor = '#25D366'
var uname = 'Boim'
var ucolor = '#fff94d'

var udialogue = [
	[
		"Oy, dah melek lu?",
	],
	[
		"Eh weekend besok mau nobar gak?",
		"Chelsea MU kan besok?",
		"Sabi lah ya, siangnya main ke rumah gue aja dulu",
	],
	[
		"Ayo lah, mumpung lagi di Jakarta gue",
		"Kapan coba terakhir ketemu",
		"Sabi gak nih?",
	],
]

var options = [
	[
		"Kayak lu udah bangun dari tadi aja",
		"Udah lah",
		"Gue bangun subuh ya, gak kayak lu",
		"Masih pagi udah berisik aja wkwkwk",
	],
	[
		"Liat dulu deh",
		"Sabi kayaknya sih",
		"MU kalah lagi juga entar, kasian elu",
		"Gak bisa gue kayaknya",
	],
	[
		"Gue kabarin ya entar",
		"Iya, liat bandar judi dulu gue",
		"Sabar napa wkwk",
		"Cielah, iya nanti dikabarin",
	]
]

var uresponse = [
	[
		[
			"Bangke, ketauan wkwk",
		],
		[
			"Gayaaa wkwkwk",
		],
		[
			"Ya abis subuh tidur lagi kan? WKWKWK",
		],
		[
			"Udah bagus gue tanyain ya wkwk",
		],
	],
	[
		[
			"Mau liat apaan dulu? Harga saham?",
		],
		[
			"Baru menang sekali aja musim lalu udah sombong",
		],
		[
			"Yaaah kenapa?",
		],
		[
			"Dikata Chelsea MU main setiap hari kali ya",
		],
	],
	[
		[
			"Yaudah deh, cepet ya",
		],
		[
			"Bangke wkwkwk, yaudah kabarin ya",
		],
		[
			"Iyalah gak sabar Chelsea kalah wkwk, yowis, kabarin ya",
		],
		[
			"Sip dah",
		],
	]
]

onready var chatLog = get_node("VBoxContainer/RichTextLabel")
onready var option1 = get_node("VBoxContainer/VBoxContainer/Option1")
onready var option2 = get_node("VBoxContainer/VBoxContainer/Option2")
onready var option3 = get_node("VBoxContainer/VBoxContainer/Option3")
onready var option4 = get_node("VBoxContainer/VBoxContainer/Option4")

var ops_picked = []
var curr_dialog_sect = 0

func _process(_delta):
	if is_complete:
		if is_choosed:
			print("Boim terpilh")
			options.append([
				"Lu kosong dari jam berapa nih? Bisa gue",
				"Sabi Im, jam berapa gue ke situ?",
				"Gue bisa nih, awas ya lu kalah mewek",
				"Ke situ ya gue besok"
			])
			uresponse.append([
				[
					"Manjiww, bebas gue gak ke mana mana kok",
				],
				[
					"Dari pagi juga sabi dah wkwkwkwk",
				],
				[
					"Ayo dah bisa diadu wkwkwk",
				],
				[
					"Naaah gitu dong wkwkwk",
					"Besok ye awas ilang luu",
				]
			])
		elif !is_choosed:
			options.append([
				"Im gue gak bisa besok",
				"Gue ada urusan lain ternyata besok",
				"Im, maap maap nih, gak bisa gue ternyata",
				"Sorry baru ngabarin Im, gue gak bisa besok"
			])
			uresponse.append([
				[
					"Yah gitu, yowis lah",
				],
				[
					"Yah yaudah deh",
				],
				[
					"Ooh oke deh, santuy",
				],
				[
					"Jah kirain bisa",
					"Yaudah deh, santai"
				]
			])
		is_complete = false
		is_finished = true
		_loop()

# Called when the node enters the scene tree for the first time.
func _ready():
	_loop()

func _loop():
	_deactivate_options()
	
	if curr_dialog_sect < 3:
		var dialogue_set = udialogue[curr_dialog_sect]
		for dialogue in dialogue_set:
			add_chat_log(uname, dialogue, ucolor)
			
		var option_set = options[curr_dialog_sect]
		add_options(option_set[0], option_set[1], option_set[2], option_set[3])
		
	elif is_finished:
		var option_set = options[options.size() - 1]
		add_options(option_set[0], option_set[1], option_set[2], option_set[3])
		is_finished = false

func add_chat_log(dname, text, color):
	chatLog.append_bbcode('\n')
	chatLog.append_bbcode('[color=' + color + ']' + dname + '[/color]')
	chatLog.append_bbcode('\n')
	chatLog.append_bbcode(text)
	chatLog.append_bbcode('\n')

func add_options(op1, op2, op3, op4):
	option1.text = op1
	option2.text = op2
	option3.text = op3
	option4.text = op4
	
	option1.connect("pressed", self, "_option1_pressed")
	option2.connect("pressed", self, "_option2_pressed")
	option3.connect("pressed", self, "_option3_pressed")
	option4.connect("pressed", self, "_option4_pressed")
	
	_activate_options()

func give_response(op):
	var dialogue_set = uresponse[curr_dialog_sect][op]
	for dialogue in dialogue_set:
		add_chat_log(uname, dialogue, ucolor)
	
	curr_dialog_sect += 1
	_loop()

func _option1_pressed():
	add_chat_log(pname, option1.text, pcolor)
	ops_picked.append(1)
	_deactivate_options()
	give_response(0)

func _option2_pressed():
	add_chat_log(pname, option2.text, pcolor)
	ops_picked.append(2)
	_deactivate_options()
	give_response(1)

func _option3_pressed():
	add_chat_log(pname, option3.text, pcolor)
	ops_picked.append(3)
	_deactivate_options()
	give_response(2)

func _option4_pressed():
	add_chat_log(pname, option4.text, pcolor)
	ops_picked.append(4)
	_deactivate_options()
	give_response(3)

func _deactivate_options():
	option1.visible = false
	option2.visible = false
	option3.visible = false
	option4.visible = false

func _activate_options():
	option1.visible = true
	option2.visible = true
	option3.visible = true
	option4.visible = true
