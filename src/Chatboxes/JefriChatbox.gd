extends Control

var is_complete = false
var is_finished = false
var is_choosed = false

var pname = 'Nugi'
var pcolor = '#25D366'
var uname = 'Pak Jefri'
var ucolor = '#34B7F1'

var udialogue = [
	[
		"Pagi mas",
		"Hari ini sibuk gak?",
	],
	[
		"Ini saya minta tolong untuk buatkan bahan untuk presentasi bisa gak?",
		"Kalau bisa, saya juga minta mas untuk ikut saya ke luar kota",
		"Untuk pekan depan",
		"Karena Mas Alif yang harusnya ikut kebetulan berhalangan",
		"Bisa kah?",
	],
	[
		"Dari kantor memang diminta untuk setidaknya dua orang untuk ke sana",
		"Dan agenda ini sangat penting",
		"Bisa tolong kabari saya nanti malam apakah bisa atau tidak?",
		"Jadi bahan presentasinya bisa mas siapkan weekend nanti",
	],
]

var options = [
	[
		"Tidak pak",
		"Lagi santai sih pak",
		"Ada apa pak?",
		"Lengang saja pak",
	],
	[
		"Hmm, boleh saya pikir-pikir dulu pak?",
		"Saya belum ada agenda yang penting sih pak",
		"Mungkin bisa pak",
		"Yang lain tidak ada yang bisa pak?",
	],
	[
		"Oke pak, nanti malam saya kabari",
		"Siap pak",
		"Oke, terima kasih juga tawarannya pak",
		"Oke pak, siap",
	]
]

var uresponse = [
	[
		[
			"Oh begitu",
		],
		[
			"Oalah",
		],
		[
			"Jadi gini",
		],
		[
			"Oh begitu",
		],
	],
	[
		[
			"Boleh, tapi jangan lama-lama mas",
		],
		[
			"Nah, bisa ikut gak ya kira-kira?",
		],
		[
			"Yah, jangan mungkin mas, bisa dipastikan gak?",
		],
		[
			"Ada aja sih",
			"Tapi jujur saya kurang percaya sama yang lain"
		],
	],
	[
		[
			"Oke, kalau bisa jangan sampai nunggu malam ya hahaha",
		],
		[
			"*thumbs up*",
		],
		[
			"Iya, tolong ya mas",
		],
		[
			"Oke",
		],
	]
]

onready var chatLog = get_node("VBoxContainer/RichTextLabel")
onready var option1 = get_node("VBoxContainer/VBoxContainer/Option1")
onready var option2 = get_node("VBoxContainer/VBoxContainer/Option2")
onready var option3 = get_node("VBoxContainer/VBoxContainer/Option3")
onready var option4 = get_node("VBoxContainer/VBoxContainer/Option4")

var ops_picked = []
var curr_dialog_sect = 0

func _process(_delta):
	if is_complete:
		if is_choosed:
			print("Pak Jefri terpilh")
			options.append([
				"Pak, saya bisa jadinya, boleh jelasin ke saya tugasnya?",
				"Maaf baru mengabari pak, saya bisa untuk tugasnya",
				"Boleh tau detail tugasnya pak? Saya bisa ikut",
				"Saya bisa buat tugasnya pak dan ke luar kota juga"
			])
			uresponse.append([
				[
					"Mantap, makasih banyak ya mas",
					"Segera setelah ini saya jelasin"
				],
				[
					"Wah makasih banyak mas",
					"Saya kirim detailnya segera ya"
				],
				[
					"Oke mas, sebentar ya",
					"Makasih banyak ya sebelumnya"
				],
				[
					"Wah mantap memang Mas Nugi",
					"Sebentar saya kirim bahan untuk dikerjakan besok ya"
				]
			])
		else:
			options.append([
				"Mohon maaf pak, saya tidak bisa, terima kasih",
				"Pak, mohon maaf saya berhalangan juga ternyata",
				"Mohon maaf pak, ternyata terlalu mendadak buat saya",
				"Maaf baru mengabari pak, saya tidak bisa"
			])
			uresponse.append([
				[
					"Yasudah",
				],
				[
					"Yah makin susah deh saya",
					"Yasudah, terima kasih ya"
				],
				[
					"Iya memang serba mendadak semua",
					"Ya sudah, makasih ya mas"
				],
				[
					"Yah saya kira bisa",
					"Ya sudah, terima kasih ya"
				]
			])
		is_complete = false
		is_finished = true
		_loop()

# Called when the node enters the scene tree for the first time.
func _ready():
	_loop()

func _loop():
	_deactivate_options()
	
	if curr_dialog_sect < 3:
		var dialogue_set = udialogue[curr_dialog_sect]
		for dialogue in dialogue_set:
			add_chat_log(uname, dialogue, ucolor)
			
		var option_set = options[curr_dialog_sect]
		add_options(option_set[0], option_set[1], option_set[2], option_set[3])
		
	elif is_finished:
		var option_set = options[options.size() - 1]
		add_options(option_set[0], option_set[1], option_set[2], option_set[3])
		is_finished = false

func add_chat_log(dname, text, color):
	chatLog.append_bbcode('\n')
	chatLog.append_bbcode('[color=' + color + ']' + dname + '[/color]')
	chatLog.append_bbcode('\n')
	chatLog.append_bbcode(text)
	chatLog.append_bbcode('\n')

func add_options(op1, op2, op3, op4):
	option1.text = op1
	option2.text = op2
	option3.text = op3
	option4.text = op4
	
	option1.connect("pressed", self, "_option1_pressed")
	option2.connect("pressed", self, "_option2_pressed")
	option3.connect("pressed", self, "_option3_pressed")
	option4.connect("pressed", self, "_option4_pressed")
	
	_activate_options()

func give_response(op):
	var dialogue_set = uresponse[curr_dialog_sect][op]
	for dialogue in dialogue_set:
		add_chat_log(uname, dialogue, ucolor)
	
	curr_dialog_sect += 1
	_loop()

func _option1_pressed():
	add_chat_log(pname, option1.text, pcolor)
	ops_picked.append(1)
	_deactivate_options()
	give_response(0)

func _option2_pressed():
	add_chat_log(pname, option2.text, pcolor)
	ops_picked.append(2)
	_deactivate_options()
	give_response(1)

func _option3_pressed():
	add_chat_log(pname, option3.text, pcolor)
	ops_picked.append(3)
	_deactivate_options()
	give_response(2)

func _option4_pressed():
	add_chat_log(pname, option4.text, pcolor)
	ops_picked.append(4)
	_deactivate_options()
	give_response(3)

func _deactivate_options():
	option1.visible = false
	option2.visible = false
	option3.visible = false
	option4.visible = false

func _activate_options():
	option1.visible = true
	option2.visible = true
	option3.visible = true
	option4.visible = true
