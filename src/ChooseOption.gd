extends Control

var opt_choosed = 0

onready var option1 = get_node("VBoxContainer/VBoxContainer/Option1")
onready var option2 = get_node("VBoxContainer/VBoxContainer/Option2")
onready var option3 = get_node("VBoxContainer/VBoxContainer/Option3")
onready var option4 = get_node("VBoxContainer/VBoxContainer/Option4")

# Called when the node enters the scene tree for the first time.
func _ready():
	option1.connect("pressed", self, "_option1_pressed")
	option2.connect("pressed", self, "_option2_pressed")
	option3.connect("pressed", self, "_option3_pressed")
	option4.connect("pressed", self, "_option4_pressed")

func _option1_pressed():
	opt_choosed = 1

func _option2_pressed():
	opt_choosed = 2

func _option3_pressed():
	opt_choosed = 3

func _option4_pressed():
	opt_choosed = 4
