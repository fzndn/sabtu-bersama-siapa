extends Control

onready var amelia_button = get_node("VBoxContainer/AmeliaDisplay")
onready var boim_button = get_node("VBoxContainer/BoimDisplay")
onready var jefri_button = get_node("VBoxContainer/JefriDisplay")

onready var amelia_chatbox = get_node("AmeliaChatBox")
onready var boim_chatbox = get_node("BoimChatBox")
onready var jefri_chatbox = get_node("JefriChatBox")

var is_complete = false
var is_finished = false

onready var choose = get_node("ChooseOption")

var is_choosed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	amelia_chatbox.visible = false
	boim_chatbox.visible = false
	jefri_chatbox.visible = false
	choose.visible = false
	
	amelia_button.connect("pressed", self, "_amelia_button_pressed")
	boim_button.connect("pressed", self, "_boim_button_pressed")
	jefri_button.connect("pressed", self, "_jefri_button_pressed")

func _amelia_button_pressed():
	amelia_chatbox.visible = true
	boim_chatbox.visible = false
	jefri_chatbox.visible = false

func _jefri_button_pressed():
	amelia_chatbox.visible = false
	boim_chatbox.visible = false
	jefri_chatbox.visible = true

func _boim_button_pressed():
	amelia_chatbox.visible = false
	boim_chatbox.visible = true
	jefri_chatbox.visible = false

func _process(_delta):
	var is_a_complete = false
	if amelia_chatbox.ops_picked.size() == 3:
		is_a_complete = true
	
	var is_j_complete = false
	if jefri_chatbox.ops_picked.size() == 3:
		is_j_complete = true
	
	var is_b_complete = false
	if boim_chatbox.ops_picked.size() == 3:
		is_b_complete = true
		
	if is_a_complete and is_b_complete and is_j_complete and !is_complete:
		is_complete = true
		
	if is_complete and !is_finished:
		amelia_chatbox.visible = false
		boim_chatbox.visible = false
		jefri_chatbox.visible = false
		
		amelia_button.disabled = true
		jefri_button.disabled = true
		boim_button.disabled = true
		
		choose.visible = true
		
		if choose.opt_choosed > 0 and !is_choosed:
			is_choosed = true
			if choose.opt_choosed == 1:
				amelia_chatbox.is_choosed = true
			elif choose.opt_choosed == 2:
				jefri_chatbox.is_choosed = true
			elif choose.opt_choosed == 3:
				boim_chatbox.is_choosed = true
			elif choose.opt_choosed == 4:
				pass
				
			amelia_chatbox.is_complete = true
			jefri_chatbox.is_complete = true
			boim_chatbox.is_complete = true
			
			choose.visible = false
			amelia_button.disabled = false
			jefri_button.disabled = false
			boim_button.disabled = false
			
			is_finished = true
