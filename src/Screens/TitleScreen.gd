extends Control

onready var start_button = get_node("Button")

# Called when the node enters the scene tree for the first time.
func _ready():
	start_button.connect("pressed", self, "_start_pressed")
	pass # Replace with function body.

func _start_pressed():
	get_tree().change_scene("res://src/Screens/MainScreen.tscn")
